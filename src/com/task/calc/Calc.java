package com.task.calc;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Hanna_Aleinik on 10/18/2016.
 */


public class Calc {


    /**
     * Calculate sum of two operands
     *
     * @param firstOperand   - the first operand
     * @param secondOperand- the second operand
     * @return sum of two operands
     */
    static float sum(final float firstOperand, final float secondOperand) {
        return firstOperand + secondOperand;
    }

    /**
     * Calculate multiplication of two operands
     *
     * @param firstOperand   - the first operand
     * @param secondOperand- the second operand
     * @return multiplication of two operands
     */
    static float multiplication(final float firstOperand, final float secondOperand) {
        return firstOperand * secondOperand;
    }

    /**
     * Calculate subtraction of two operands
     *
     * @param firstOperand   - the first operand
     * @param secondOperand- the second operand
     * @return subtraction of two operands
     */
    static float subtraction(final float firstOperand, final float secondOperand) {
        return firstOperand - secondOperand;
    }

    /**
     * Calculate division of two operands
     *
     * @param firstOperand  - the first operand
     * @param secondOperand - the second operand
     * @return division of two operands
     * @throws Exception - division by 0
     */
    static float division(final float firstOperand, final float secondOperand) throws Exception {
        if (secondOperand == 0)
            throw new Exception("Incorrect operation: Division by 0");
        return firstOperand / secondOperand;
    }

    /**
     * Method read and convert user input for the operation
     *
     * @param indexOfOperand - the number of operand
     * @return the entered value of user that was converted to float
     */
    static float getOperand(final String indexOfOperand) {
        boolean isOperandValid = true;
        float operand = 0;
        BufferedReader readerOfKeyBoard = new BufferedReader(new InputStreamReader(System.in));
        while (isOperandValid) {
            try {
                System.out.println("Enter " + indexOfOperand + " operand:");
                String userInput = readerOfKeyBoard.readLine();
                operand = Float.parseFloat(userInput);
                isOperandValid = false;
            } catch (Exception e) {
                System.out.println("You enter invalid operand. Please enter valid one... ");
            }
        }
        return operand;
    }

    /**
     * Method verify that user enters valid operation
     *
     * @param userInput - user input
     * @return true - if user entered invalid value for operation
     * false - if user entered valid value for operation
     */
    private static boolean isInputInvalid(final int userInput) {
        return (userInput >= 1) && (userInput <= 5);
    }

    /**
     * Enter the manipulation that user wants to do
     * user is able to select the following operations:
     * 1: Sum;
     * 2: Multiplication;
     * 3: Subtraction;
     * 4-Division;
     * 5 - Exit
     *
     * @return int user choice
     */
    private static int getUserChoice() {
        int operationNumber = 0;
        boolean isOperationValid = false;
        BufferedReader readerOfKeyBoard = new BufferedReader(new InputStreamReader(System.in));
        while (!isOperationValid) {
            System.out.println("Enter operation: 1 - Sum; 2 - Multiplication; 3 - Subtraction; 4 - Division; 5 - Exit");
            try {
                String userChoice = readerOfKeyBoard.readLine();
                operationNumber = Integer.parseInt(userChoice);
                boolean inputValidation = isInputInvalid(operationNumber);
                if (!inputValidation) {
                    System.out.println("You enter invalid operation. Please enter valid operation...");
                } else {
                    isOperationValid = true;
                }
            } catch (Exception e) {
                System.out.println("You enter invalid operation. Please enter valid operation...");
            }
        }
        return operationNumber;
    }


    public static void main(String[] args) throws Exception {
        System.out.println("Hello! You run the calculation. ");
        while (true) {
            float operationResult = 0;
            int operationNumber = getUserChoice();
            if (operationNumber == 5) {
                break;
            }
            // entering operands
            float firstOperand = getOperand("first");
            float secondOperand = getOperand("second");

            // execute the selected operation with entered values
            try {
                switch (operationNumber) {
                    case 1: {
                        operationResult = sum(firstOperand, secondOperand);
                        break;
                    }
                    case 2: {
                        operationResult = multiplication(firstOperand, secondOperand);
                        break;
                    }
                    case 3: {
                        operationResult = subtraction(firstOperand, secondOperand);
                        break;
                    }
                    case 4: {
                        operationResult = division(firstOperand, secondOperand);
                        break;
                    }
                }
                System.out.println("The operationResult of operation:  " + operationResult);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}





